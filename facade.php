<?php

class Sneakers
{
    public function chooseSneakers() {
        echo 'Кроссовки;' . '<br>';
    }
}

class Pants
{
    public function choosePants() {
        echo 'Штаны;' . '<br>';
    }
}

class Jacket
{
    public function chooseJacket() {
        echo 'Куртка;' . '<br>';
    }
}

class Hat
{
    public function chooseHat() {
        echo 'Шапка;' . '<br>';
    }
}

class Consultant
{
    protected Sneakers $sneakers;
    protected Pants $pants;
    protected Jacket $jacket;

    public function __construct()
    {
        $this->sneakers = new Sneakers();
        $this->pants = new Pants();
        $this->jacket = new Jacket();
    }

    public function chooseClothes ()
    {
        $sneakers = $this->sneakers;
        $pants = $this->pants;
        $jacket = $this->jacket;

        $sneakers->chooseSneakers();
        $pants->choosePants();
        $jacket->chooseJacket();
    }
}

$consultant = new Consultant();
$consultant->chooseClothes();

$hat = new Hat();
$hat->chooseHat();