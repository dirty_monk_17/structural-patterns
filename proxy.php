<?php

interface Sneakers
{
    public function run(string $color);
}

class WhiteSneakers implements Sneakers
{
    public function run(string $color)
    {
        echo 'Надев ' . $color . ' кроссовки, бегаем по СТАДИОНУ' . '<br>';
    }
}

class ColoredSneakers implements Sneakers
{
    public function run(string $color)
    {
        echo 'Надев ' . $color . ' кроссовки, бегаем ГДЕ УГОДНО' . '<br>';
    }
}

class SneakersProxy implements Sneakers
{
    public Sneakers $sneakers;

    public function run(string $color)
    {
        if ($color !== 'белые') {
            $this->sneakers = new ColoredSneakers();
        } else {
            $this->sneakers = new WhiteSneakers();
        }
        $this->sneakers->run($color);
    }
}

$sneakers = new SneakersProxy();
$sneakers->run('белые');
$sneakers->run('красные');
$sneakers->run('черные');