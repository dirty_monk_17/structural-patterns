<?php

interface HatInterface
{
    public function getPrice(): int;
}

class Hat implements HatInterface
{
    private int $price;

    public function __construct(int $price)
    {
        $this->price = $price;
    }

    public function getPrice(): int
    {
        return $this->price;
    }
}

class Bag implements HatInterface
{
    public array $bag = [];
    public int $price = 0;

    public function getPrice(): int
    {
        return $this->price;
    }

    public function addItem(HatInterface $item)
    {
        $this->bag[] = $item;
        $this->price += $item->getPrice();
    }
}

$hat1 = new Hat(10);
$hat2 = new Hat(15);

$bag1 = new Bag();

$bag1->addItem($hat1);
$bag1->addItem($hat2);

$hat4 = new Hat(20);
$bag2 = new Bag();

$bag2->addItem($bag1);
$bag2->addItem($hat4);

echo $bag2->getPrice();
