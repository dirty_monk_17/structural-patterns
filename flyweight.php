<?php

class Product
{
    protected string $title;
    protected int $price;
    protected ProductBrand $brand;

    public function __construct($title, $price, $brand)
    {
        $this->title = $title;
        $this->price = $price;
        $this->brand = $brand;
    }
}

class ProductBrand
{
    protected string $brandName;
    protected string $brandLogo;

    public function __construct($brandName, $brandLogo)
    {
        $this->brandName = $brandName;
        $this->brandLogo = $brandLogo;
    }
}

class ShoppingCart
{
    protected array $products = [];

    public function addProduct($title, $price, $brandName, $brandLogo)
    {
        $this->products[] = ProductFactory::getProduct($title, $price, $brandName, $brandLogo);
    }

    public function getProducts(): array
    {
        return $this->products;
    }
}

class ProductFactory
{
    public static array $brandTypes = [];

    public static function getProduct($title, $price, $brandName, $brandLogo): Product
    {
        $brand = static::getBrand($brandName, $brandLogo);

        return new Product($title, $price, $brand);
    }

    public static function getBrand($brandName, $brandLogo): ProductBrand
    {
        if (isset(static::$brandTypes[$brandName])) {
            return static::$brandTypes[$brandName];
        }

        return static::$brandTypes[$brandName] = new ProductBrand($brandName, $brandLogo);
    }
}

$cart = new ShoppingCart();
$cart->addProduct('sports shoes', 120, 'NIKE', 'nike.png');
$cart->addProduct('kids shoes', 100, 'NIKE', 'nike.png');
$cart->addProduct('women shoes', 110, 'NIKE', 'nike.png');
$cart->addProduct('run shoes', 140, 'ASICS', 'asics.png');
$cart->addProduct('day shoes', 90, 'ADIDAS', 'adidas.png');

echo 'Всего товаров: ' . count($cart->getProducts());
echo '<br>';
echo 'Всего брендов: ' . count(ProductFactory::$brandTypes);
echo '<br>';
echo '<br>';
echo '1 повторяющийся бренд (объект бренда) используется ' .  (count($cart->getProducts()) - count(ProductFactory::$brandTypes)) . ' раза.';