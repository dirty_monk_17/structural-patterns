<?php

interface Driver
{
    public function getName(): string;
    public function startEngine();
    public function startDriving();
    public function crash();
}

class Maxim implements Driver
{
    private string $name = "Максим";

    public function startEngine()
    {
        echo 'Бр бр бр бр брб р бр ' . '<br>';
    }

    public function startDriving()
    {
        echo 'Не знает, что ему надо делать ' . '<br>';
    }

    public function crash()
    {
        echo 'Упал на бок, даже не стартанув... ' . '<br>';
    }

    public function getName(): string
    {
        return $this->name;
    }
}

class Alex implements Driver
{
    private string $name = "Саша";

    public function startEngine()
    {
        echo 'Дыр дыр дыр дыр дыр дыр ' . '<br>';
    }

    public function startDriving()
    {
        echo 'Воткнул первую передачу и дал газу ' . '<br>';
    }

    public function crash()
    {
        echo 'ДТП невозможно! ' . '<br>';
    }

    public function getName(): string
    {
        return $this->name;
    }
}

abstract class Vehicle
{
    protected Driver $driver;

    public function __construct(Driver $driver)
    {
        $this->driver = $driver;
    }

    public function changeDriver(Driver $driver)
    {
        $this->driver = $driver;
    }

    abstract public function drive();
}

class Bike extends Vehicle
{
    public function drive()
    {
        echo $this->driver->getName() . ' сел на мотоцикл ' . '<br>';
        $this->driver->startEngine();
        $this->driver->startDriving();
        $this->driver->crash();
    }
}

class Car extends Vehicle
{
    public function drive()
    {
        echo $this->driver->getName() . ' сел в машину ' . '<br>';
        $this->driver->startEngine();
        $this->driver->startDriving();
    }
}

$driverAlex = new Alex();
$driverMaks = new Maxim();
$bike = new Bike($driverAlex);
$car = new Car($driverMaks);

$bike->drive();
echo '<br>';
$car->drive();
echo '<br>';

$bike->changeDriver($driverMaks);
$car->changeDriver($driverAlex);

$bike->drive();
echo '<br>';
$car->drive();
echo '<br>';