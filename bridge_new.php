<?php
interface IPrinter
{
    public function printHeader($textHeader);
    public function printBody($textBody);
}

class PdfPrinter implements IPrinter
{
    public function printHeader($textHeader) {
        echo 'Это ваш заголовок (' . $textHeader. ') в PDF файле<br>';
    }

    public function printBody($textBody) {
        echo 'Это ваш текст (' . $textBody. ') в pdf файле<br>';
    }
}

class ExcelPrinter implements IPrinter
{
    public function printHeader($textHeader) {
        echo 'Это ваш заголовок (' . $textHeader. ') в XLS файле<br>';
    }

    public function printBody($textBody) {
        echo 'Это ваш текст (' . $textBody. ') в XLS файле<br>';
    }
}

abstract class Report
{
    protected IPrinter $printer;

    public function __construct(IPrinter $printer) {
        $this->printer = $printer;
    }

    public function printHeader($textHeader) {
        $this->printer->printHeader($textHeader);
    }

    public function printBody($textBody) {
        $this->printer->printBody($textBody);
    }
}

class WeeklyReport extends Report
{
    public function print(array $text) {
        $this->printHeader($text['header']);
        $this->printBody($text['body']);
    }
}

$report = new WeeklyReport(new ExcelPrinter());
$report->print(['header' => 'мой заголовок для EXCEL', 'body' => 'мой текст для EXCEL']); // This is your header (my header for excel) in the xls file</ br>This is your text body (my body for excel) in the xls file<br>
$report = new WeeklyReport( new PdfPrinter());
$report->print(['header' => 'мой заголовок для PDF', 'body' => 'мой текст для PDF']); // This is your header (my header for pdf) in the pdf file</ br>This is your text body (my body for pdf) in the pdf file<br>