<?php

interface StreetWorkout
{
    public function competeWithBars();
}

class BarsCompetition implements StreetWorkout
{
    public function competeWithBars()
    {
        echo 'Уличный спортсмен соревнуется на брусьях.' . '<br>';
    }
}

class StreetSportsman
{
    public function showCompete(StreetWorkout $competition)
    {
        $competition->competeWithBars();
    }
}

interface PowerLifting
{
    public function competeWithBarbell();
}

class BarbellCompetition implements PowerLifting
{
    public function competeWithBarbell()
    {
        echo 'Уличный спортсмен соревнуется по жиму штанги.' . '<br>';
    }
}

class Adapter implements StreetWorkout
{
    private BarbellCompetition $competition;

    public function __construct(BarbellCompetition $competition)
    {
        $this->competition = $competition;
    }

    public function competeWithBars()
    {
        echo 'Приняв анаболические стероиды, ';
        $this->competition->competeWithBarbell();
    }
}

$petr = new StreetSportsman();
$barbell = new BarbellCompetition();
$bars = new BarsCompetition();
$adapter = new Adapter($barbell);

$petr->showCompete($bars);
$petr->showCompete($adapter);