<?php

interface Notifier
{
    public function send(string $message): string;
}

class BaseNotifier implements Notifier
{
    public function send(string $message): string
    {
        return $message . '<br>';
    }
}

class NotifierDecorator implements Notifier
{
    private Notifier $notifier;

    public function __construct(Notifier $notifier)
    {
        $this->notifier = $notifier;
    }

    public function send(string $message): string
    {
        return $this->notifier->send($message);
    }
}

class SMSDecorator extends NotifierDecorator
{
    public function send(string $message): string
    {
        return 'SMS: ' . parent::send($message);
    }
}

class Privat24Decorator extends NotifierDecorator
{
    public function send(string $message): string
    {
        return 'Privat24: ' . parent::send($message);
    }
}

class ViberDecorator extends NotifierDecorator
{
    public function send(string $message): string
    {
        return 'Viber: ' . parent::send($message);
    }
}

$baseNotifier = new BaseNotifier();

$sms = new SMSDecorator($baseNotifier);
$privat = new Privat24Decorator($baseNotifier);
$viber = new ViberDecorator($baseNotifier);

echo $sms->send('Зачисление на карту 1000.00 грн');
echo $privat->send('Зачисление на карту 2000.00 грн');
echo $viber->send('Зачисление на карту 3000.00 грн');